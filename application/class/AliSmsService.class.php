<?php


class AliSmsService
{
    protected $config = [];

    public function __construct()
    {
        $this->config = [
            'AccessKeyId' => 'LTAIawKYENkZVbm8',
            'SignName' => '学生点名',
            'AccessKeySecret' => 'JRx4qIH4T0Wkxqi3wL5sdCHBA3QxBm',
        ];
    }

    /**
     * @return string
     */
    protected function uuid()
    {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        }
        mt_srand((float) microtime() * 10000); //optional for php 4.2.0 and up.随便数播种，4.2.0以后不需要了。
            $charid = strtoupper(md5(uniqid(rand(), true))); //根据当前时间（微秒计）生成唯一id.
            $hyphen = chr(45); // "-"
            $uuid = ''. //chr(123)// "{"
                substr($charid, 0, 8).$hyphen.substr($charid, 8, 4).$hyphen.substr($charid, 12, 4).$hyphen.substr($charid, 16, 4).$hyphen.substr($charid, 20, 12);
        //.chr(125);// "}"
        return $uuid;
    }

    /**
     * @param array $param
     *
     * @return bool
     */
    public function sendSMS(array $param)
    {
        list($mobile, $ParamString, $TemplateCode) = $param;

        $Params['Action'] = 'SendSms'; //操作接口名，系统规定参数，取值：SingleSendSms
        $Params['RegionId'] = 'cn-hangzhou'; //机房信息
        $Params['AccessKeyId'] = $this->config['AccessKeyId']; //阿里云颁发给用户的访问服务所用的密钥ID
        $Params['Format'] = 'JSON'; //返回值的类型，支持JSON与XML。默认为XML
        $Params['TemplateParam'] = rawurlencode($this->paramString($ParamString)); //短信模板中的变量；数字需要转换为字符串；个人用户每个变量长度必须小于15个字符。
        $Params['PhoneNumbers'] = $mobile; //目标手机号
        $Params['SignatureMethod'] = 'HMAC-SHA1'; //签名方式，目前支持HMAC-SHA1
        $Params['SignatureNonce'] = $this->uuid(); //唯一随机数
        $Params['SignatureVersion'] = '1.0'; //签名算法版本，目前版本是1.0
        $Params['SignName'] = rawurlencode($this->config['SignName']); //管理控制台中配置的短信签名（状态必须是验证通过）
        $Params['TemplateCode'] = $TemplateCode; //管理控制台中配置的审核通过的短信模板的模板CODE（状态必须是验证通过）
        $Params['Timestamp'] = rawurlencode(gmdate("Y-m-d\TH:i:s\Z")); //请求的时间戳。日期格式按照ISO8601标准表示，
        //并需要使用UTC时间。格式为YYYY-MM-DDThh:mm:ssZ
        $Params['Version'] = '2017-05-25'; //API版本号，当前版本2016-09-27
        ksort($Params);
        $PostData = '';
        foreach ($Params as $k => $v) {
            $PostData .= $k.'='.$v.'&';
        }
        $PostData .= 'Signature='.rawurlencode(base64_encode(hash_hmac('sha1', 'GET&%2F&'.rawurlencode(substr($PostData, 0, -1)), $this->config['AccessKeySecret'].'&', true)));

        return $this->cURL($PostData);
    }

    /**
     * @param $url
     *
     * @return bool
     */
    protected function cURL($url)
    {
        $url = 'http://dysmsapi.aliyuncs.com/?'.$url;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($data, true);

        return $res;
    }

    /**
     * @param $ParamString
     *
     * @return string
     */
    protected function paramString($ParamString)
    {
        if (is_array($ParamString)) {
            return json_encode($ParamString);
        }
    }
}
